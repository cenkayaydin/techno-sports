import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import SportsPage from "./views/SportsPage.vue"
import AppPage from "./views/AppPage.vue"
import PricingPage from "./views/PricingPage.vue"

Vue.use(Router)

export default new Router({
  //mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/sports',
      name: 'sports',
      component: SportsPage
    },
    {
      path: '/app',
      name: 'app',
      component: AppPage
    },
    {
      path: '/pricing',
      name: 'pricing',
      component: PricingPage
    }
  ]
})
